# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR UBports Developers
# This file is distributed under the same license as the account-plugins package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: account-plugins 0.12\n"
"Report-Msgid-Bugs-To: https://gitlab.com/ubports/development/core/lomiri-"
"online-accounts-plugins/-/issues\n"
"POT-Creation-Date: 2024-08-21 19:58+0000\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: az\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: data/providers/facebook.provider.in.in:3
#: data/services/facebook-microblog.service.in:4
#: data/services/facebook-sharing.service.in:4
msgid "Facebook"
msgstr ""

#: data/providers/flickr.provider.in.in:3
#: data/services/flickr-microblog.service.in:4
#: data/services/flickr-sharing.service.in:4
msgid "Flickr"
msgstr ""

#: data/providers/foursquare.provider.in.in:3
#: data/services/foursquare-microblog.service.in:4
msgid "Foursquare"
msgstr ""

#: data/providers/google.provider.in.in:3
msgid "Google"
msgstr ""

#: data/providers/google.provider.in.in:4
msgid "Includes Gmail, Google Docs, Google Photos and YouTube"
msgstr ""

#: data/providers/identica.provider.in.in:3
#: data/services/identica-microblog.service.in:4
msgid "identi.ca"
msgstr ""

#: data/providers/instagram.provider.in.in:3
#: data/services/instagram-microblog.service.in:4
msgid "Instagram"
msgstr ""

#: data/providers/linkedin.provider.in.in:3
#: data/services/linkedin-microblog.service.in:4
msgid "LinkedIn®"
msgstr ""

#: data/providers/mcloud.provider.in.in:3
msgid "mcloud"
msgstr ""

#: data/providers/microsoft.provider.in.in:3
msgid "Microsoft"
msgstr ""

#: data/providers/nextcloud.provider.in.in:3
msgid "Nextcloud"
msgstr ""

#: data/providers/owncloud.provider.in.in:3
msgid "ownCloud"
msgstr ""

#: data/providers/sina.provider.in.in:3
#: data/services/sina-microblog.service.in:4
msgid "Sina"
msgstr ""

#: data/providers/sohu.provider.in.in:3
#: data/services/sohu-microblog.service.in:4
msgid "Sohu"
msgstr ""

#: data/providers/x.provider.in.in:3 data/services/x-microblog.service.in:4
msgid "X (formerly known as Twitter)"
msgstr ""

#: data/providers/vk.provider.in.in:3
msgid "VKontakte"
msgstr ""

#: data/services/google-drive.service.in:4
msgid "GoogleDrive"
msgstr ""

#: data/services/google-im.service.in:4
msgid "GoogleTalk"
msgstr ""

#: data/services/picasa.service.in:4
msgid "Picasa"
msgstr ""

#: qml/google/Main.qml:16
msgid ""
"Google requires logging in via the system web-browser. Press the button "
"below to start."
msgstr ""

#: qml/google/Main.qml:21
msgid "Open Morph Web-browser"
msgstr ""

#: qml/google/Main.qml:28 qml/nextcloud/NewAccount.qml:90
#: qml/owncloud/NewAccount.qml:89
msgid "Cancel"
msgstr ""

#: qml/nextcloud/NewAccount.qml:15 qml/owncloud/NewAccount.qml:15
msgid "Invalid host URL"
msgstr ""

#: qml/nextcloud/NewAccount.qml:16 qml/owncloud/NewAccount.qml:191
msgid "Invalid username or password"
msgstr ""

#: qml/nextcloud/NewAccount.qml:39 qml/owncloud/NewAccount.qml:38
msgid "URL:"
msgstr ""

#: qml/nextcloud/NewAccount.qml:45
msgid "https://example.com/nextcloud"
msgstr ""

#: qml/nextcloud/NewAccount.qml:54 qml/owncloud/NewAccount.qml:53
msgid "Username:"
msgstr ""

#: qml/nextcloud/NewAccount.qml:60 qml/owncloud/NewAccount.qml:59
msgid "Your username"
msgstr ""

#: qml/nextcloud/NewAccount.qml:69 qml/owncloud/NewAccount.qml:68
msgid "Password:"
msgstr ""

#: qml/nextcloud/NewAccount.qml:75 qml/owncloud/NewAccount.qml:74
msgid "Your password"
msgstr ""

#: qml/nextcloud/NewAccount.qml:96 qml/owncloud/NewAccount.qml:95
msgid "Continue"
msgstr ""

#: qml/owncloud/NewAccount.qml:44
msgid "https://example.com/owncloud"
msgstr ""
