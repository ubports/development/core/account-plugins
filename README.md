# Account plugins for Lomiri Online Accounts

This project contains the account configuration plugins for the credentials
configuration panel.

These plugins are responsible for creating and configuring online accounts.

## Dependencies

The plugins depend on liblomiri-online-accounts-plugin from lomiri-online-account.

## Licence

The plugins are licensed under the GNU GPL version 2.

For bundled artwork licenses see data/icons/README.md.

## Resources

https://gitlab.com/ubports/development/core/lomiri-online-accounts-plugins

## i18n: Translating Lomiri's Account Plugins into your Language

You can easily contribute to the localization of this project (i.e. the
translation into your language) by visiting (and signing up with) the
Hosted Weblate service:
https://hosted.weblate.org/projects/lomiri/lomiri-online-accounts-plugins

The localization platform of this project is sponsored by Hosted Weblate
via their free hosting plan for Libre and Open Source Projects.
